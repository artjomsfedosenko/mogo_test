<?php

require 'src/Model/CarAddsModel.php';
require 'src/Service/CarAddDataRetriever.php';

$dataRetriever =new \App\Service\CarAddDataRetriever();

$make = "Land Rover";

$maxPrice = $dataRetriever->getMaxPrice($make);
var_dump($maxPrice);

$minPrice = $dataRetriever->getMinPrice($make);
var_dump($minPrice);

$avgPrice = $dataRetriever->getAveragePriceForMake($make);
var_dump($avgPrice);

$avgPriceModel = $dataRetriever->getAveragePriceForMakeModel($make, "Discovery III");
var_dump($avgPriceModel);