CREATE SCHEMA IF NOT EXISTS car_adds;

CREATE USER application_user@localhost IDENTIFIED BY 'qwerty';
GRANT ALL PRIVILEGES ON car_adds.* TO application_user@localhost;

CREATE TABLE car_adds(
   `id` INT NOT NULL AUTO_INCREMENT,
   `addId` INT NOT NULL,
   `year` INT NOT NULL,
   `make` VARCHAR(64) NOT NULL,
   `model` VARCHAR(64) NOT NULL,
   `price` DECIMAL(10,2) NOT NULL,
   PRIMARY KEY (`id`),
   INDEX `make` (`make`),
   INDEX `make_model` (`make`, `model`),
   INDEX `addId` (`addId`)
);
