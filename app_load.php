<?php

use App\Service\CarAddsFetcher;

require 'src/Service/CarAddsFetcher.php';
require 'src/Service/CarAddsParser.php';
require 'src/Model/CarAddsModel.php';
require 'src/Entity/CarAdd.php';

for($i = 100; $i < 102; $i++){
    print_r("Processing page: " . $i);

    $url = "https://www.cheki.co.ke/vehicles?page={$i}";

    $fetcher = new CarAddsFetcher();
    $fetcher->fetch($url);

    print_r("Page {$i} processed. Sleeping");

    sleep(2);
}

