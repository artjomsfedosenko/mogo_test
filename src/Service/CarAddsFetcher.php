<?php

namespace App\Service;

use App\Entity\CarAdd;
use App\Model\CarAddsModel;


class CarAddsFetcher
{
    /** @var CarAddsModel */
    private $carAddsModel;
    /** @var CarAddsParser */
    private $carAddsParser;

    public function __construct()
    {
        $this->carAddsParser = new CarAddsParser();
        $this->carAddsModel = new CarAddsModel();
    }

    public function fetch(string $url) : void
    {
        $rawHtml = $this->getRawHtml($url);
        $carAdds = $this->carAddsParser->parse($rawHtml);

        /** @var CarAdd $carAdd */
        foreach ($carAdds as $carAdd){
            if(!$this->isAddProcessed($carAdd) && $this->isAddValid($carAdd)){
                $this->carAddsModel->save($carAdd);
            }
        }
    }

    private function getRawHtml(string $url) : string
    {
        $rawHtml = file_get_contents($url);
        if($rawHtml === false){
            return "";
            //Write to log
        }

        return $rawHtml;
    }

    private function isAddProcessed(CarAdd $add) : bool
    {
        return $this->carAddsModel->recordExists($add->getAddId());
    }

    private function isAddValid(CarAdd $add) : bool
    {
        return $add->getMake() !== "" &&
                $add->getModel() !== "" &&
                $add->getYear() !== "" &&
                $add->getPrice() !== 0;
    }
}