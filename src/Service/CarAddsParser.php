<?php

namespace App\Service;

use App\Entity\CarAdd;
use DOMDocument;
use DOMElement;
use DOMNode;

class CarAddsParser
{
    private const ADDS_LIST_TAG_NAME = "ul";
    private const ADD_LIST_CLASS_NAME = "listing-unit__container";
    private const ID_ATTRIBUTE_NAME = "data-listing-id";
    private const CAR_STING_CONTAINER_CLASS_NAME = "card-header";
    private const PRICE_CLASS_NAME = "listing-price";

    /*
     * This array of hardcoded multi-word car makes is here for the sake of simplicity
     * In real world some other source of retrieving car make name should be found or
     * exceptions should be stored in more appropriate way
     */
    private $carMakeExceptions = ["Land Rover", "Alpha Romeo", "Mercedes-Benz"];

    public function parse(string $rawHtml) : array
    {
        $html = new DOMDocument();
        @$html->loadHTML($rawHtml);

        $addsList = $this->getAddsListElement($html);

        $carAdds = [];
        foreach ($addsList->getElementsByTagName("li") as $carAddData){
            if($carAdd = $this->parseCarAdd($carAddData)){
                $carAdds[] = $carAdd;
            }
        }

        return $carAdds;
    }

    private function getAddsListElement(DOMDocument $html) : ?DOMElement
    {
        $possibleLists = $html->getElementsByTagName(self::ADDS_LIST_TAG_NAME);

        /** @var DOMNode $childNode */
        foreach ($possibleLists as $childNode){
            if($this->isAddsListContainer($childNode)){
                return $childNode;
            }
        }

        return null;
    }

    private function hasClassAttribute(DOMNode $node) : bool
    {
        if($node->attributes->getNamedItem("class")){
            return true;
        }

        return false;
    }

    private function isAddsListContainer(DOMNode $node) : bool
    {
        return $this->elementHasClass($node, self::ADD_LIST_CLASS_NAME);
    }

    private function isCarDataStringContainer(DOMNode $node) : bool
    {
        return $this->elementHasClass($node, self::CAR_STING_CONTAINER_CLASS_NAME);
    }

    private function isPriceContainer(DOMNode $node) : bool
    {
        return $this->elementHasClass($node, self::PRICE_CLASS_NAME);
    }

    private function elementHasClass(DOMNode $node, string $className) : bool
    {
        if(!$this->hasClassAttribute($node)){
            return false;
        }

        $elementClassName = $node->attributes->getNamedItem("class")->nodeValue;
        if($elementClassName === $className){
            return true;
        }

        return false;
    }

    private function parseCarAdd(DOMElement $carAddData) : ?CarAdd
    {
        if($addId = $carAddData->getAttribute(self::ID_ATTRIBUTE_NAME)){
            //Year Make Model
            $carDataString = $this->getCarDataString($carAddData);

            $year = $this->extractYear($carDataString);
            $make = $this->extractMake($carDataString);
            $model = $this->extractModel($carDataString, $year, $make);
            $price = $this->extractPrice($carAddData);

            return new CarAdd($addId, $make, $model, $year, $price);
        }

        return null;
    }

    private function getCarDataString(DOMElement $carAddData) : string
    {
        //Get all divs
        $divElements = $carAddData->getElementsByTagName("div");

        /** @var DOMElement $div */
        foreach ($divElements as $div){
            if($this->isCarDataStringContainer($div)){
                $linkElement = $div->getElementsByTagName("a")->item(0);

                return $linkElement->nodeValue;
            }
        }

        return "";
    }

    private function extractYear(string $carDataString) : string
    {
        preg_match("/[0-9]{4}/", $carDataString, $matches);
        if(($year = $matches[0]) != null){
            return  $year;
        }

        return "";
    }

    private function extractMake(string $carDataString) : string
    {
        //Check if car name is amongst the exclusions
        foreach ($this->carMakeExceptions as $carMakeException){
            if(strpos($carDataString, $carMakeException) !== false){
                return $carMakeException;
            }
        }

        preg_match("/[A-Za-z]+/", $carDataString, $matches);
        if(($make = $matches[0]) != null){
            return $make;
        }

        return "";
    }

    private function extractModel(string $carDataString, string $year, string $make) : string
    {
        $carDataString = strtr($carDataString, [$year => "", $make => ""]);
        $carDataString = trim($carDataString);
        $parts = preg_split("/(\d{1}\.\d{1})/", $carDataString);
        if($parts[0] != null){
            return trim($parts[0]);
        }

        return "";
    }

    private function extractPrice(DOMElement $carAddData) : float
    {
        $possiblePriceElements = $carAddData->getElementsByTagName("h2");
        /** @var DOMNode $element */
        foreach ($possiblePriceElements as $element){
            if($this->isPriceContainer($element)){
                $price = trim($element->nodeValue);
                $price = strtr($price, ["KSh" => "", "," => ""]);

                return $price;
            }
        }

        return 0;
    }
}