<?php


namespace App\Service;


use App\Model\CarAddsModel;

class CarAddDataRetriever
{
    /** @var CarAddsModel */
    private $carAddsModel;

    public function __construct()
    {
        $this->carAddsModel = new CarAddsModel();
    }

    public function getAveragePriceForMake(string $make) : ?float
    {
        return $this->carAddsModel->getAveragePriceForMake($make);
    }

    public function getAveragePriceForMakeModel(string $make, string $model) : ?float
    {
        return $this->carAddsModel->getAveragePriceForMakeModel($make, $model);
    }

    public function getMaxPrice(string $make) : ?float
    {
        return $this->carAddsModel->getMaxPrice($make);
    }

    public function getMinPrice(string $make) : ?float
    {
        return $this->carAddsModel->getMinPrice($make);
    }
}