<?php

namespace App\Entity;


class CarAdd
{
    private $addId;

    private $make;

    private $model;

    private $year;

    private $price;

    /**
     * CarAdd constructor.
     * @param $addId
     * @param $make
     * @param $model
     * @param $year
     * @param $price
     */
    public function __construct($addId, $make, $model, $year, $price)
    {
        $this->addId = $addId;
        $this->make = $make;
        $this->model = $model;
        $this->year = $year;
        $this->price = $price;
    }


    /**
     * @return mixed
     */
    public function getAddId()
    {
        return $this->addId;
    }

    /**
     * @param mixed $addId
     */
    public function setAddId($addId): void
    {
        $this->addId = $addId;
    }

    /**
     * @return mixed
     */
    public function getMake()
    {
        return $this->make;
    }

    /**
     * @param mixed $make
     */
    public function setMake($make): void
    {
        $this->make = $make;
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param mixed $model
     */
    public function setModel($model): void
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param mixed $year
     */
    public function setYear($year): void
    {
        $this->year = $year;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }
}