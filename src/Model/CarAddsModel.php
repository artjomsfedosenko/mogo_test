<?php

namespace App\Model;

use App\Entity\CarAdd;
use Exception;
use PDO;

class CarAddsModel
{
    private static $pdo;

    private const TABLE_NAME = 'car_adds';

    public function save(CarAdd $carAdd) : void
    {
        $connection = $this->getPdo();

        try{
            $connection->beginTransaction();
            $insertCarAddStatement = $connection->prepare(
                "INSERT IGNORE INTO " . self::TABLE_NAME . " (`addId`, `year`, `make`, `model`, `price`) VALUES (?, ?, ?, ?, ?);"
            );

            $insertCarAddStatement->execute(
                [$carAdd->getAddId(), $carAdd->getYear(), $carAdd->getMake(), $carAdd->getModel(), $carAdd->getPrice()]
            );

            $connection->commit();
        }catch(Exception $e){
            $connection->rollBack();
            //Write to log
        }
    }

    public function recordExists(string $addId) : bool
    {
        $connection = $this->getPdo();
        $sql = "SELECT EXISTS (SELECT 1 FROM " . self::TABLE_NAME . " WHERE `addId` = ?);";

        try{
            $statement = $connection->prepare($sql);
            $statement->execute([$addId]);

            $resultSet = $statement->fetchAll();
        }catch(Exception $e){
            //If something went wrong we assume that add was processed and don`t go further
            //Write to log
            return true;
        }

        if(reset($resultSet[0]) === 1){
            return true;
        }

        return false;
    }

    public function getAveragePriceForMake(string $make) : float
    {
        $connection = $this->getPdo();
        $sql = "SELECT AVG(`price`) AS avgPrice FROM " . self::TABLE_NAME . " WHERE `make` = ?;";

        try{
            $statement = $connection->prepare($sql);
            $statement->execute([$make]);
            $resultSet = $statement->fetch();
        }catch(Exception $e){
            //Write to log
            return null;
        }

        if($resultSet === false || is_null($resultSet["avgPrice"])){
            return null;
        }

        return $resultSet["avgPrice"];
    }

    public function getAveragePriceForMakeModel(string $make, string $model) : float
    {
        $connection = $this->getPdo();
        $sql = "SELECT AVG(`price`) AS avgPrice FROM " . self::TABLE_NAME . " WHERE `make` = ? AND `model` = ?;";

        try{
            $statement = $connection->prepare($sql);
            $statement->execute([$make, $model]);
            $resultSet = $statement->fetch();
        }catch(Exception $e){
            //Write to log
            return null;
        }

        if($resultSet === false || is_null($resultSet["avgPrice"])){
            return null;
        }

        return $resultSet["avgPrice"];
    }

    public function getMaxPrice(string $make) : ?float
    {
        $connection = $this->getPdo();
        $sql = "SELECT MAX(`price`) AS maxPrice FROM " . self::TABLE_NAME . " WHERE `make` = ?;";

        try{
            $statement = $connection->prepare($sql);
            $statement->execute([$make]);
            $resultSet = $statement->fetch();
        }catch(Exception $e){
            //Write to log
            return 0;
        }

        if($resultSet === false || is_null($resultSet["maxPrice"])){
            return 0;
        }

        return $resultSet["maxPrice"];
    }

    public function getMinPrice(string $make) : ?float
    {
        $connection = $this->getPdo();
        $sql = "SELECT MIN(`price`) AS minPrice FROM " . self::TABLE_NAME . " WHERE `make` = ?;";

        try{
            $statement = $connection->prepare($sql);
            $statement->execute([$make]);
            $resultSet = $statement->fetch();
        }catch(Exception $e){
            //Write to log
            return null;
        }

        if($resultSet === false || is_null($resultSet["minPrice"])){
            return null;
        }

        return $resultSet["minPrice"];
    }

    private function getPdo() : PDO
    {
        if (!self::$pdo){
            try{
                self::$pdo = new PDO(
                    "mysql:host=localhost;dbname=car_adds;charset=utf8",
                    "application_user",
                    "qwerty",
                    [
                        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                        PDO::ATTR_EMULATE_PREPARES => false,
                    ]);
            }catch(Exception $e){
                //Write to some kind of log
            }
        }

        return self::$pdo;
    }
}